#ifndef BOARD_H
#define BOARD_H

#include "Game.h"
#include "GameAi.h"
#include "GameMap.h"
#include "GameServer.h"
#include "Help.h"
#include "MyGraphicsScene.h"
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMainWindow>
#include <QPushButton>
#include <QSignalMapper>
#include <QWidget>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui {
    class Board;
}
QT_END_NAMESPACE

class Board : public QMainWindow {
    Q_OBJECT

public:
    explicit Board(QWidget* parent = nullptr, GAMEMODE gameMode = GAMEMODE::LOCAL,
                   QString const& player1_name = "", QString player2_name = "");

    ~Board() override;

    Game* getGame();

    void resizeEvent(QResizeEvent* event) override;

signals:
    void sendServerMessage(QString message);

public slots:
    void onFieldSelection(QPointF);
    void writeGameMessage();
    void writeErrorMessage();
    void writeServerMessage(QString const& message);
    void up_scene();
    void on_btnHelp_clicked();
    void on_btnBack_clicked();

private:
    Ui::Board* ui;
    Game* game;
    MyGraphicsScene* m_scene;
    Help* help = nullptr;
    GAMEMODE game_mode;
    Board(const Board&) = delete;
    Board& operator=(const Board&) = delete;
    Board(Board&&) = delete;
    Board& operator=(Board&&) = delete;

signals:
    void clickedBack();
};
#endif // BOARD_H
