#ifndef HELP_H
#define HELP_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui {
    class Help;
}
QT_END_NAMESPACE

class Help : public QDialog {
    Q_OBJECT

public:
    explicit Help(QWidget* parent = nullptr);

    ~Help() override;

private:
    Ui::Help* ui;
    Help(const Help&) = delete;
    Help& operator=(const Help&) = delete;
    Help(Help&&) = delete;
    Help& operator=(Help&&) = delete;
};
#endif // HELP_H
