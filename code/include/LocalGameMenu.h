#ifndef LOCALGAMEMENU_H
#define LOCALGAMEMENU_H

#include "Board.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
    class LocalGameMenu;
}
QT_END_NAMESPACE

class LocalGameMenu : public QMainWindow {
    Q_OBJECT

public:
    explicit LocalGameMenu(QWidget* parent = nullptr);

    ~LocalGameMenu() override;

private slots:
    void on_btnStartGame_clicked();
    void on_btnBackLocalClicked();

private:
    Ui::LocalGameMenu* ui;
    Board* board;
    LocalGameMenu(const LocalGameMenu&) = delete;
    LocalGameMenu& operator=(const LocalGameMenu&) = delete;
    LocalGameMenu(LocalGameMenu&&) = delete;
    LocalGameMenu& operator=(LocalGameMenu&&) = delete;

signals:
    void back();
};
#endif // LOCALGAMEMENU_H
