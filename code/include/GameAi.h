#ifndef GAMEAI_H
#define GAMEAI_H

#include "Game.h"
#include "GameMap.h"
#include "Lib.h"
#include "Player.h"

class GameAI : public Game {
public:
    GameAI(Player* p1, Player* p2, GAMEMODE gameMode);
    ~GameAI();

    void playMove(Player* player, int index, MyGraphicsScene* scene) override;

    void playSetupMoveAI(MyGraphicsScene* scene);
    void playMovingMoveAI(MyGraphicsScene* scene);
    void playMillAI(MyGraphicsScene* scene);

    // Helper functions
    bool turnAI();
    Player* getPlayerAI();
    Player* getPlayerHuman();
    FIELDSTATE getHumanFieldstate();

    // Minimax phase 1
    std::pair<int, int> maxSetup(int depth, int remainingPieces, int alfa, int beta);
    std::pair<int, int> minSetup(int depth, int remainingPieces, int alfa, int beta);

    void makeSetupMoveAI(Player* player, int i);
    void revertSetupMoveAI(Player* player, int i);
    int heuristicSetup();

    // Minimax phase 2
    std::tuple<int, int, int> maxPlay(int depth, int alfa, int beta);
    std::tuple<int, int, int> minPlay(int depth, int alfa, int beta);

    void makePlayMoveAI(Player* player, int moveFrom, int moveTo);
    void revertPlayMoveAI(Player* player, int moveFrom, int moveTo);

private:
    // Max depth for minimax algorithm in each phase, best values for a quick AI play
    int maxDepthPhase1 = 5;
    int maxDepthPhase2 = 7;

    FIELDSTATE playerAI;
    GameAI(const GameAI&) = delete;
    GameAI& operator=(const GameAI&) = delete;
    GameAI(GameAI&&) = delete;
    GameAI& operator=(GameAI&&) = delete;
};
#endif // GAMEAI_H
