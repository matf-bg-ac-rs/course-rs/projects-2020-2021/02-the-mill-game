#ifndef MAINMENU_H
#define MAINMENU_H

#include "Board.h"
#include "LocalGameMenu.h"
#include "MultiplayerMenu.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
    class MainMenu;
}
QT_END_NAMESPACE

class MainMenu : public QMainWindow {
    Q_OBJECT

public:
    explicit MainMenu();
    ~MainMenu() override;

private slots:
    void on_localPlayBtn_clicked();
    void on_serverPlayBtn_clicked();
    void on_aiPlayBtn_clicked();

public slots:
    void on_btnBackMainClicked();

private:
    Ui::MainMenu* ui;
    MultiplayerMenu* mp_menu = nullptr;
    LocalGameMenu* lg_menu = nullptr;
    MainMenu(const MainMenu&) = delete;
    MainMenu& operator=(const MainMenu&) = delete;
    MainMenu(MainMenu&&) = delete;
    MainMenu& operator=(MainMenu&&) = delete;
};
#endif // MAINMENU_H
