#ifndef MULTIPLAYERMENU_H
#define MULTIPLAYERMENU_H

#include "Board.h"
#include "GameServer.h"
#include "TcpServer.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
    class MultiplayerMenu;
}
QT_END_NAMESPACE

class MultiplayerMenu : public QMainWindow {
    Q_OBJECT

public:
    explicit MultiplayerMenu(QWidget* parent = nullptr, GAMEMODE mode = GAMEMODE::AI);

    ~MultiplayerMenu() override;

private slots:
    void on_connectBtn_clicked();
    void on_btnBackMultiplayerClicked();

private:
    Ui::MultiplayerMenu* ui;
    GAMEMODE mode;
    Board* board;
    TcpServer* server;
    MultiplayerMenu(const MultiplayerMenu&) = delete;
    MultiplayerMenu& operator=(const MultiplayerMenu&) = delete;
    MultiplayerMenu(MultiplayerMenu&&) = delete;
    MultiplayerMenu& operator=(MultiplayerMenu&&) = delete;

signals:
    void back();
};
#endif // MULTIPLAYERMENU_H
